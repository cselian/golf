var gameTabber;
var tabberOptions = {
	'onLoad': function(argsObj) {
		if (argsObj.tabber.id == 'game') {
			gameTabber = argsObj.tabber;
			showGameTab(2);
			$('.tabbernav li').last().hide();
		}
	}
};

function showGameTab(page)
{
	gameTabber.tabShow("" + page);
}

function showOrHide($what, $show)
{
	if ($show) $what.show(); else $what.hide();
}

function makeReadonly($selector, $read)
{
	if ($read) $($selector).each(function(e) {
		$(this).addClass('readonly')
		$(this).attr('readonly', 'readonly');
	});
	else $($selector).each(function(e) {
		$(this).removeClass('readonly')
		$(this).removeAttr('readonly');
	});
}

function toggleEditor($lnk)
{
	$lnk.toggleClass('expanded');
	$show = $lnk.hasClass('expanded');
	$ed = $lnk.next('.ed-cont');
	showOrHide($ed, $show);
	return $show;
}
