// Score = Sum(score) - handicap
// Par score = course par - handicap
// in progress ->
//		par score = sum(played pars) - handicap / 18
//		score = sum(playes scores) - handicap / 18
function detailSet(closing)
{
	if (closing)
	{
		detailField.val('TODO');
		detailSummary.val('Summary');
		//summarizeDetail();
		return;
	}
	
	if (detailField.val() == '' || detailField.val() == 'null')
	{
		detailSummary.val('Not Set');
		return;
	}
}

jQuery(function($) {
	$('#gm_players').change(function(e) { changePlayerCount(); });
	$('.players .plr-name').change(function(e) { changePlayerName($(this)); });
	makeReadonly('.holes .name', 1);
	makeReadonly('.score .plr-name', 1);
	changePlayerCount();
	registerHoleEditor();
});

var playerMaxCount = 4;
function changePlayerCount()
{
	var $cnt = parseInt($('#gm_players').val());
	for(i = 1; i <= playerMaxCount; i++)
	{
		$show = i <= $cnt;
		showOrHide($('.players .plr-' + i).parent().parent(), $show);
		showOrHide($('.score .plr-' + i).parent().parent(), $show);
		$colIx = 3 + i;
		$sel = '.holes td:nth-child(' + $colIx + '), .holes th:nth-child(' + $colIx + ')';
		if ($show) $($sel).show(); else $($sel).hide(); 
	}
}

function changePlayerName($fld)
{
	$ix = $('.rowindex', $fld.parent().parent()).val();
	setValueIn('.holes', '', '.plr-name-' + $ix, $fld.val())
	setValueIn('.score', '.plr-' + $ix, '.plr-name', $fld.val())
}

function setValueIn($cont, $ix, $on, $val)
{
	if ($ix != '') {
		$cont = $($cont + ' ' + $ix).parent().parent();
	} else {
		$cont = $($cont);
	}
	$($on, $cont).val($val);
}

function registerHoleEditor()
{
	//TODO: is gm_places needed?
	$('#gm_places').change(function(e){
		$detl = getPlaceDetail();
		$ed = $('#hole-details');
		$show = $detl == null;
		showOrHide($ed, $show);
		makeReadonly('.holes .hole-par', !$show);
		makeReadonly('.holes .hole-index', !$show);
		showNumUpper($show);
		if (!$show)
		{
			var holes = (';' + $detl).split(';');
			for(i = 1; i <= 18; i++)
			{
				var pix = holes[i].split(',');
				var txt = getHoleTxt(i, false);
				txt.val(pix[0]);
				txt = getHoleTxt(i, true);
				txt.val(pix[1]);
			}
		}
	});
	$('#score-hole').change(function(e){
		$ix = parseInt($(this).val());
		$('#score-par').val(getHoleTxt($ix, false).val());
		$('#score-idx').val(getHoleTxt($ix, true).val());
		$row = $('.holes .hole-' + $ix).closest('tr');
		$('.plr-strokes', $('.score .plr-1').closest('tr')).val($('.hole-player-1', $row).val())
		$('.plr-strokes', $('.score .plr-2').closest('tr')).val($('.hole-player-2', $row).val())
		$('.plr-strokes', $('.score .plr-3').closest('tr')).val($('.hole-player-3', $row).val())
		$('.plr-strokes', $('.score .plr-4').closest('tr')).val($('.hole-player-4', $row).val())
		makeReadonly('#score-prev', $ix == 1);
		makeReadonly('#score-next', $ix == 18);
		$('#score-player').val(1);
		$plrIndex = 1;
	});
	$('#score-prev').click(function(e) {
		$('#score-hole').val(parseInt($('#score-hole').val()) - 1);
		$('#score-hole').trigger('change');
	});
	$('#score-next').click(function(e) {
		$('#score-hole').val(parseInt($('#score-hole').val()) + 1);
		$('#score-hole').trigger('change');
	});
	$('#raw-regenerate').click(function(e) {
		setRawDetails();
	});
	$('#players_ed_lnk').click(function(e) {
		$show = toggleEditor($(this));
		makeReadonly('.players .plr-name', !$show);
		makeReadonly('.players .plr-handicap', !$show);
		makeReadonly('.players .plr-member-code', !$show);
	});
	if ($('#holes-ed').length == 0) {
		makeReadonly('.holes .hole-par'  , true);
		makeReadonly('.holes .hole-index', true);
	}
	$('#num_ed a').click(function(e) {
		e.preventDefault();
		if ($('#score-hole').length)
		{
			$ix = parseInt($('#score-hole').val());
			$num = $(this).attr('href').substring(1);
			$row = $('.holes .hole-' + $ix).closest('tr');
			$plrIndex = parseInt($('#score-player').val());
			$('.plr-strokes', $('.score .plr-' + $plrIndex).closest('tr')).val($num);
			$('.hole-player-' + $plrIndex, $row).val($num);
			//TODO: should it go to next hole automatically $('#score-next').trigger('click');
			$lastPlayer = $plrIndex == $('#gm_players').val();
			if ($lastPlayer && $ix == 18) { $('.tabbernav li').last().show(); setRawDetails(); }
			$('#score-player').val($lastPlayer ? 1 : $plrIndex + 1);
			return;
		}
		$hole = $('#holes-ed').val();
		if ($hole != '')
		{
			$hole = parseInt($hole);
			$idx = $('#holes-set-what_1').is(':checked');
			$txt = getHoleTxt($hole, $idx);
			$num = $(this).attr('href').substring(1);
			$txt.val($num);
			if ($hole != 18) $('#holes-ed').val($hole + 1);
		}
	});
	$('#holes-set-1').click(function(e) {
		$('#holes-ed').val('1');
	});
	$('#holes-set-what input').change(function(e){
		$('#holes-set-1').trigger('click');
		showNumUpper(null);
	});
	$('#score-hole').trigger('change');
	$('#players_ed_lnk').trigger('click');
	$('#holes_ed_lnk').trigger('click');
}

function setRawDetails()
{
	$txt = 'Name	Handicap	MemCode	Score	Par Score	Rank\r\n';
	$plrCnt = parseInt($('#gm_players').val());
	$holeRow = '\r\n\r\nHole	Par	Index';
	for($ix = 1; $ix <= $plrCnt; $ix++)
	{
		$tr = $('.players .plr-' + $ix).closest('tr');
		$name = $('.plr-name', $tr).val();
		if ($name == '') $name = 'Player' + $ix;
		$holeRow += '	' + $name;
	}
	$txt += $holeRow + '\r\n';
	//TODO: Player Summary
	for($ix = 1; $ix <= 18; $ix++)
	{
		$holeRow = $ix + '	' + getHoleTxt($ix, false).val() + '	' + getHoleTxt($ix, true).val()
		$row = $('.holes .hole-' + $ix).closest('tr');
		$holeRow += '	' + $('.hole-player-1', $row).val();
		if ($plrCnt > 1) $holeRow += '	' + $('.hole-player-2', $row).val();
		if ($plrCnt > 2) $holeRow += '	' + $('.hole-player-3', $row).val();
		if ($plrCnt > 3) $holeRow += '	' + $('.hole-player-4', $row).val();
		$txt += $holeRow + '\r\n';
	}
	$('#rawData').val($txt);
}

function setParDetails()
{
	$txt = 'Hole	Par	Index\r\n';
	for($i = 1; $i <= 18; $i++)
		$txt += $i + '	' + getHoleTxt($i, false).val() + '	' + getHoleTxt($i, true).val() + '\r\n';
	$('#parDetails').val($txt);
}

function showNumUpper($show)
{
	if ($show == null) $show = $('#hole-details').is(":visible");
	$idx = $('#holes-set-what_1').is(':checked');
	showOrHide($('#num_ed_upper'), $show && $idx);
}

function getHoleTxt($hole, $idx)
{
	return $('.holes .hole-' + ($idx ? 'index' : 'par') + ':eq(' + ($hole -1) + ')');
}