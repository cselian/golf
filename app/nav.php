<?php
function nav_link($url, $text, $page)
{
	$css = $url == $page || ($url == '' && $page == 'home') ? 'class="current"' : '';
	link_r(site_url($url, 1), $text, 0, $css);
	print_nl();
}

function nav_links($page, $data = 0)
{
	if (user('admin'))
		$links = array('' => 'Home', 'members', 'par', 'logout');
	else if (user())
		$links = array('' => 'Home', 'games', 'add', 'logout');
	else
		$links = array('' => 'Home');
	if ($data) return $links;
	foreach ($links as $key=>$value)
	{
		if (is_numeric($key))
		{
			$key = $value;
			$value = ucfirst($value);
		}
		 
		nav_link($key, $value, $page);
	}
	userInfo();
}

function nav_title($page)
{
	if ($page == 'home')
		return sprintf('%s - %s', site_var('site_title'), site_var('site_byline'));

	$links = nav_links(null, 1);
	foreach ($links as $key=>$value)
	{
		if ($page == (is_numeric($key) ? $value : $key))
		{
			$name = is_numeric($key) ? ucfirst($value) : $value;
			return sprintf('%s - %s', $name, site_var('site_title'));
		}
	}
}
?>
