<?php
function user($what = 'loggedin', $val = false)
{
	if ($what == 'loggedin')
		return isset($_SESSION['name']);
	if ($val === null)
		unset($_SESSION[$what]);
	else if ($val !== false)
		$_SESSION[$what] = $val;
	else
		return isset($_SESSION[$what]) ? $_SESSION[$what] : false;
}

function userInfo()
{
	if (!user()) return;
	$clubs = site_var('clubs');
	echo '<br/>' . user('name') . ' (' . $clubs[user('club')]['name'] . ')';
	if (user('email')) echo ' - ' . user('email');
}

function buildTable($cols = null, $css = null)
{
	include_once 'ci_table.php';
	$table = new CI_Table();
	$table->set_template(array(
		'table_open' => '<table class="grid'.($css != null ? ' ' : '') . $css.'">',
		'heading_row_start' => '<tr class="head">',
		'row_start' => '<tr class="odd">',
		'row_alt_start' => '<tr class="even">'
	));
	
	if ($cols != null) $table->set_heading($cols);
	
	return $table;
}

?>