<?php
class Actions extends Controller
{
	function pages($page)
	{
		if (($page == 'members' || $page == 'par') && !user('admin')) die('Unauthorized Admin Access');
		if (($page == 'games' || $page == 'add') && !user()) die('Unauthorized Member Access');

		if ($page == 'par' && isset($_POST['parDetails']))
			Model::save_par($_POST['parDetails']);
		if ($page == 'add' && isset($_POST['rawData'])) {
			Model::save_game($_POST['rawData']);
			header('Location: ' . site_url('games', 1));
		}

		$this->data['page'] = $page;
		$this->set_head('title', nav_title($page));
		$this->set_view('page');

		$this->data['content'] = self::pageFile($page, 0);
	}

	private static function pageFile($name, $content = 1)
	{
		return site_file('app/pages/' .$name. '.php', $content);
	}

	function login($club = false)
	{
		if (!$club) { header('Location: ' . site_url('', 1)); return; }
		$this->data['page'] = 'login';
		$this->data['name'] = $this->data['phone'] = '';
		$err = false;
		if (isset($_POST['memberno']))
		{
			$this->data['name'] = $_POST['memberno'];
			$this->data['phone'] = $_POST['phoneno'];
			$u = Model::get_member($club, $_POST['memberno']);
			if ($u == false) {
				$err = 'name';
			} else if ($u['phoneno'] != $_POST['phoneno']) {
				$err = 'phone';
			} else {
				if (isset($u['admin'])) user('admin', true);
				if (isset($u['email'])) user('email', $u['email']);
				user('club', $club);
				user('name', isset($u['name']) ? $u['name'] : $_POST['memberno']);
				header('Location: ' . site_url(isset($u['admin']) ? 'members' : 'games', 1));
			}
		}

		$this->data['error'] = $err;
	}

	function logout()
	{
		user('name', null);
		user('club', null);
		user('admin', null);
		header('Location: ' . site_url('', 1));
		die();
	}

	private function needsLogOn()
	{
		if (user())
			return false;
		header('Location: ' . site_url('login', 1));
		return true;
	}
}
?>
