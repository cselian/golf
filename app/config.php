<?php
$settings['site_name'] = 'Cselian TG';
$settings['site_copy'] = '&copy; 2013 - %s Cselian Technology Group';
$settings['site_title'] = 'CTG Golf';
$settings['site_byline'] = 'Golf Scorecards';
$settings['site_description'] = '';
$settings['site_keywords'] = '';
$settings['bucketaction'] = 'pages';
$settings['singlemodule'] = 1;
$settings['contactemail'] = 'imran@cselian.com';
$settings['includes'] = array('nav', 'model', 'app', 'SportsHelper', 'CHtml');
$settings['clubs'] = array(
	'bgc' => array('name' => 'Bangalore Golf Club', 'admin' => 'beegecc'),
	'kga' => array('name' => 'Karnataka Golf Association', 'admin' => 'kagaa'),
);
?>
