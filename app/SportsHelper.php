<?php
class SportsHelper {

static function tbl($end = 0)
{
	self::$ix = 0;
	echo '
' . ($end ? '</table>' : '<table border="1" class="grid">');
}

private static $ix;

static function tr($head, $cell)
{
	echo sprintf('
	<tr class="%s"><th>%s</th><td>%s</td></tr>
', self::$ix % 2 ? 'odd2' : 'even2', $head, $cell);
	self::$ix++;
}

private static $firstTab = true;

static function tab($txt, $cls = '')
{
	if (self::$firstTab) echo '<div class="tabber" id="game">'; else echo '</div>';
	if ($txt == 'end') self::$firstTab == true; else self::$firstTab = false;
	echo $txt == 'end' ? '</div>' : sprintf('<div class="tabbertab %s"><h3>%s</h3>', $cls, $txt);
}

static function msg($txt, $tab = null, $tab2 = null)
{
	$fmt = '<a href="javascript:showGameTab(%s);">';
	if ($tab != null) $txt = str_replace('{tab}', sprintf($fmt, $tab - 1), $txt);
	if ($tab2 != null) $txt = str_replace('{tab2}', sprintf($fmt, $tab2 - 1), $txt);
	echo sprintf('<p class="msg">%s</p>', $txt) . PHP_EOL;
}

static function boxes($names, $ix, $pfx, $vals = false)
{
	$res = array(sprintf('<input type="hidden" value="%s" class="rowindex %s" /> %s', $ix, $pfx . '-' . $ix, $ix));
	foreach ($names as $i=>$n)
		$res[] = sprintf('<input type="text" class="%s" value="%s" />',
			$pfx . '-' . str_replace(' ', '-', strtolower($n)), $vals && isset($vals[$ix][$i]) ? $vals[$ix][$i]: '');
	return $res;
}

static function tab_ed($what)
{
	if ($what == 'end') echo '</div>';
	else echo sprintf('<a href="#" id="%s_ed_lnk" class="ed-link expandable">editor</a>' . PHP_EOL .
		'<div id="%s_ed" class="ed-cont">', $what, $what);
}

static function num_ed($data)
{
	extract(array_merge(array('idSuffix' => '', 'from' => 1, 'to' => 9, 'after' => ''), $data));
	$op = sprintf('<div id="num_ed%s">', $idSuffix);
	for ($i = $from; $i <= $to; $i++)
		$op .= sprintf('<a href="#%s">%s</a>', $i, $i);
	return $op . $after . '</div>';
}

} ?>
