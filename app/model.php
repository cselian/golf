<?php
class Model
{
	public static function get_member($club, $name)
	{
		if ($name == 'admin') {
			$clubs = site_var('clubs');
			return array('phoneno' => $clubs[$club]['admin'], 'admin' => true);
		}

		$name = strtolower($name);
		$mems = tsv_to_array(site_file(sprintf('app/data/%s/_members.tsv', $club), 1));
		foreach ($mems as $m) if (strtolower($m[0]) == $name)
			return array('name' => $m[0], 'phoneno' => $m[1], 'email' => $m[2]);

		return false;
	}

	public static function get_games()
	{
		$file = site_file(sprintf('app/data/%s/%s.txt', user('club'), user('name')), 1);
		return $file ? tsv_to_array($file) : array();
	}

	public static function save_game($data)
	{
		$ix = 1;
		$notExists = false;

		$fol = site_file('data/' . user('club') . date('/Y'));
		if (!is_dir($fol)) mkdir($fol);
		$fol .= date('/m/');
		if (!is_dir($fol)) mkdir($fol);

		$fol = site_file('data/' . user('club') . '/');
		while($notExists == false)
		{
			$name = date('Y/m/d-') . user('name') . ($ix == 1 ? '' : '-' . $ix);
			$file = $fol . $name . '.tsv';
			$notExists = !file_exists($file);
			$ix++;
		}

		//write file
		file_put_contents($file, $data);

		//write log
		$log = site_file(sprintf('app/data/%s/%s.txt', user('club'), user('name')));
		$exist = file_exists($log) ? file_get_contents($log) : '';
		file_put_contents($log, $exist . $name . PHP_EOL);

		//TODO: send email
	}

	public static function get_par()
	{
		$file = site_file(sprintf('app/data/%s/_golf.tsv', user('club')), 1);
		return $file ? tsv_to_array($file) : array();
	}

	public static function save_par($tsv)
	{
		$file = site_file(sprintf('app/data/%s/_golf.tsv', user('club')));
		file_put_contents($file, $tsv);
	}
}
?>
