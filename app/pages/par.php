<?php
if (isset($_POST['parDetails']))
{
	echo 'Par/Index Details for all holes of Golf Course saved';
	return;
}
?>
<form method="post">
<textarea id="parDetails" name="parDetails" style="display: none;width: 100%;" rows="20"></textarea>
<input type="submit" value="Save" onclick="setParDetails();" />
</form>
<?php
$holeList = array();
for ($ix = 1; $ix <= 18 ; $ix++) $holeList[$ix] = $ix;
$holes = array('Hole', 'Par', 'Index');

echo '<div id="hole-details">';
echo 'Set: ' . CHtml::radioButtonList('holes-set-what', 'par', array('par' => 'Par', 'idx' => 'Index'),
	array('separator' => ' ', 'template' => '{input} {label}')) . '.<br/>';
echo 'Hole: ' . CHtml::dropDownList('holes-ed', 1, $holeList) . ' <a href="#" id="holes-set-1">reset</a>';
$afterData = array('idSuffix' => '_upper', 'from' => 10, 'to' => 18);
echo SportsHelper::num_ed(array('after' => SportsHelper::num_ed($afterData))) . '</div>';

$tbl = buildTable($holes, 'holes ed-small');

unset($holes[0]);
$par = Model::get_par();
for ($ix = 1; $ix <= 18 ; $ix++)
	$tbl->add_row(SportsHelper::boxes($holes, $ix, 'hole', $par));
echo $tbl->generate();
?>
