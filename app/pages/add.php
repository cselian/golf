<?php
$players = array(1 => 1, 2 => 2, 3 => 3, 4 => 4); // same as playerMaxCount in editGolf.js
$holeList = array();
for ($ix = 1; $ix <= 18 ; $ix++) $holeList[$ix] = $ix;

SportsHelper::tab('Player(s)', 'players');
SportsHelper::msg('Enter player names and handicap then review/enter {tab}hole details</a>.', 2);

SportsHelper::tab_ed('players');
	echo 'No Of Players:' . CHtml::dropDownList('gm_players', '2', $players);
SportsHelper::tab_ed('end');

$cols = array('Sl', 'Name', 'Handicap', 'Member Code', 'Score', 'Par Score', 'Rank');
$holes = array('Hole', 'Par', 'Index');
$pnames = array('', '', '');
$tbl = buildTable($cols, 'players ed-small');
unset($cols[0]);
foreach ($players as $ix) {
	$tbl->add_row(SportsHelper::boxes($cols, $ix, 'plr'));
	$holes[] = 'Player ' . $ix;
	$pnames[] = '<input type="text" class="name plr-name-'.$ix.'" />';
}
echo $tbl->generate();

SportsHelper::tab('Holes', 'holes');
SportsHelper::msg('Review / Enter Hole details then go {tab}overleaf</a> to start play.', 3);

$par = Model::get_par();
$tbl = buildTable($holes, 'holes ed-small');
$tbl->add_row($pnames);
unset($holes[0]);
for ($ix = 1; $ix <= 18 ; $ix++)
	$tbl->add_row(SportsHelper::boxes($holes, $ix, 'hole', $par));
echo $tbl->generate();

SportsHelper::tab('Score', 'score');
SportsHelper::msg('Enter Score Hole by Hole and then go to {tab}Player Score</a>.', 1);
$tbl = buildTable(array(), 'ed-small');
	$tbl->add_row('<b>Hole</b>', CHtml::dropDownList('score-hole', 18, $holeList) .
		CHtml::button('<', array('id' => 'score-prev')) . ' ' . 
		CHtml::button('>', array('id' => 'score-next')));
	$tbl->add_row('<b>Player</b>', CHtml::dropDownList('score-player', 1, $players));
	$tbl->add_row('<b>Data Entry</b>', SportsHelper::num_ed(array()));
	$tbl->add_row('<b>Par</b>', CHtml::textField('score-par', ''));
	$tbl->add_row('<b>Index</b>', CHtml::textField('score-idx', ''));
	echo $tbl->generate();

$cols = array('Sl', 'Name', 'Adjusted Par', 'Strokes', 'Score');
$tbl = buildTable($cols, 'ed-small');
unset($cols[0]);
foreach ($players as $ix) {
	$tbl->add_row(SportsHelper::boxes($cols, $ix, 'plr'));
}
echo $tbl->generate();

SportsHelper::tab('Raw', 'raw');
echo '<form method="post">' . PHP_EOL;
echo CHtml::button('regenerate', array('id' => 'raw-regenerate')) . ' ' .
	CHtml::button('submit', array('type' => 'submit')) . '<br/><br/>';
echo '<textarea type="text" id="rawData" name="rawData" rows="28" style="width: 100%" readonly></textarea>';
echo '</form>' . PHP_EOL;

SportsHelper::tab('end');
?>
