<p>Welcome to our small website for club members to keep track of their golf games.</p>

<?php if (user()) {
	echo 'Welcome ' . user('name');
} else {
	echo '<p>Choose a club you belong to:<br>';
	foreach (site_var('clubs') as $code => $info)
		echo sprintf('<a href="%s">%s</a><br>' . PHP_EOL, site_url('login/' . $code, 1), $info['name']);
	echo '</p>';
} ?>
