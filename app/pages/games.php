<?php
$baseUrl = site_url('data/' . user('club'), 1) . '/';
$actions = explode('/', $_GET['action'], 2);
if (count($actions) > 1 && $actions[1] != '')
{
	echo '<h2>Game ' . $actions[1] . '</h2>' . PHP_EOL;
	echo sprintf('<a href="%s">download</a> or <a href="%s">go back</a><br/>'. PHP_EOL,
		$baseUrl . $actions[1] . '.tsv', site_url('games', 1));
	$op = site_file(sprintf('data/%s/%s.tsv', user('club'), $actions[1]), 1);
	echo $op !== false ? '<textarea rows="26" style="width: 100%; margin-top: 15px;">' .
		$op . '</textarea>' : 'Not Found. Click on Games link  go back';
	return;
}

$games = array_reverse(Model::get_games()); //print_r($games);

$gameUrl = site_url('games/', 1);

$last = '';
foreach ($games as $g)
{
	$g = $g[0];
	$bits = explode('/', $g);
	$curr = $bits[0] . '-' . $bits[1];
	if ($last != $curr) echo '<h2>' . $curr . '</h2>' . PHP_EOL;
	$last = $curr;
	echo sprintf('<a href="%s">%s</a> <a href="%s">download</a><br/>'. PHP_EOL,
		$gameUrl . $g, $bits[2], $baseUrl . $g . '.tsv');
}
?>